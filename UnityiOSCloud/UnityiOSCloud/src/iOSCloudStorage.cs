using UnityiOSCloud.CloudKit;
using UnityiOSCloud.KeyValue;

namespace UnityiOSCloud
{
	public class iOSCloudStorage
	{
		private static iOSCloudStorage Instance { get; set; }

		private CloudKitStorage CloudKitInternal { get; set; }
		private KeyValueStorage KeyValueInternal { get; set; }

		static iOSCloudStorage()
		{
			Instance = new iOSCloudStorage();
		}
		
		private iOSCloudStorage()
		{
			CloudKitInternal = new CloudKitStorage();
			KeyValueInternal = new KeyValueStorage();
		}

		public static CloudKitStorage CloudKit
		{
			get
			{
				return Instance.CloudKitInternal;
			}
		}
		
		public static KeyValueStorage KeyValue
		{
			get
			{
				return Instance.KeyValueInternal;
			}
		}
	}
}