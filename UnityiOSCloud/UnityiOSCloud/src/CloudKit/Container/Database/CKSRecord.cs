﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityiOSCloud.CloudKit.Container.Database
{
    [Serializable]
    public class CKSRecord : ISerializationCallbackReceiver
    {   
        [SerializeField]
        private CKSRecordID _recordId;

        [SerializeField]
        private string _recordType;
        
        [SerializeField]
        private string _recordChangeTag;

        [SerializeField]
        private List<iOSCloudFieldPair> _fields;
        
        private Dictionary<string, iOSCloudFieldPair> _fieldPairsDict;

        public CKSRecord(CKSRecordID recordId, string type)
        {
            Initialize(recordId, type);
        }
        
        public CKSRecord(string name, string type)
        {
            CKSRecordID recordId = new CKSRecordID(name, CKSZone.DEFAULT_ZONE_NAME);
            Initialize(recordId, type);
        }
        
        public CKSRecord(string name, string zoneName, string type)
        {
            CKSRecordID recordId = new CKSRecordID(name, zoneName);
            Initialize(recordId, type);
        }

        private void Initialize(CKSRecordID recordId, string type)
        {
            _recordId = recordId;
            _recordType = type;
            
            _fields = new List<iOSCloudFieldPair>();
        }

        internal void Synchronize()
        {
            _fieldPairsDict = new Dictionary<string, iOSCloudFieldPair>();

            iOSCloudFieldPair pair;
            for (int i = 0; i < _fields.Count; i++)
            {
                pair = _fields[i];
                _fieldPairsDict.Add(pair.Key, pair);
            }
        }

        public void Set(string key, object value)
        {
            if (!_fieldPairsDict.ContainsKey(key))
            {
                iOSCloudFieldPair pair = new iOSCloudFieldPair(key, value.ToString());
                _fieldPairsDict.Add(key, pair);
                _fields.Add(pair);
                return;
            }
            
            _fieldPairsDict[key].Set(value.ToString());
        }

        public object Get(string key)
        {
            if (!_fieldPairsDict.ContainsKey(key))
            {
                return null;
            }

            return _fieldPairsDict[key].Value;
        }
        
        public object this[string key]
        {
            get { return Get(key); }
            set { Set(key, value); }
        }
        
        public bool Remove(string key)
        {
            if (!_fieldPairsDict.ContainsKey(key))
            {
                return false;
            }

            iOSCloudFieldPair pair = _fieldPairsDict[key];
            _fieldPairsDict.Remove(key);
            _fields.Remove(pair);
            
            return true;
        }

        public CKSRecordID Id
        {
            get { return _recordId; }
        }

        public string Type
        {
            get { return _recordType; }
        }
        
        public string ChangeTag
        {
            get { return _recordChangeTag; }
        }
        
        public override string ToString()
        {
            return string.Format("[CKSRecord]: {0}", JsonUtility.ToJson(this));
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            Synchronize();
        }
    }
}