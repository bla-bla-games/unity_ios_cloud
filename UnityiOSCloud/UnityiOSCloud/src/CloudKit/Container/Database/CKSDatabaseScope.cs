﻿namespace UnityiOSCloud.CloudKit.Container.Database
{
	public enum CKSDatabaseScope
	{
		Public = 1,
		Private,
		Shared
	}
}