using System;
using UnityEngine;

namespace UnityiOSCloud.CloudKit.Container.Database
{
	[Serializable]
	public class CKSRecordID
	{
		[SerializeField]
		private string _recordName;
		
		[SerializeField]
		private string _zoneName;

		public CKSRecordID(string recordName, string zoneName)
		{
			_recordName = recordName;
			_zoneName = zoneName;
		}
		
		public CKSRecordID(string recordName) : this(recordName, CKSZone.DEFAULT_ZONE_NAME)
		{
		}
		
		public override string ToString()
		{
			return string.Format("[CKSRecordID(RecordName: {0}, ZoneName: {1})]", RecordName, ZoneName);
		}

		public string RecordName
		{
			get { return _recordName; }
			set { _recordName = value; }
		}

		public string ZoneName
		{
			get { return _zoneName; }
			set { _zoneName = value; }
		}
	}
}