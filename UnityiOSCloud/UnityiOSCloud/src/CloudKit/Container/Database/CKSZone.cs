using System;
using UnityEngine;

namespace UnityiOSCloud.CloudKit.Container.Database
{
	[Serializable]
	public class CKSZone
	{
		public const string DEFAULT_ZONE_NAME = "_defaultZone";
		
		[SerializeField]
		private string _zoneName;

		[SerializeField]
		private string _zoneOwner;

		public CKSZone(string zoneName)
		{
			_zoneName = zoneName;
		}
		
		public string Name
		{
			get { return _zoneName; }
		}
		
		public string Owner
		{
			get { return _zoneOwner; }
		}
		
		public override string ToString()
		{
			return string.Format("[CKSZone]: {0}", JsonUtility.ToJson(this));
		}
	}
}