﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityiOSCloud.CloudKit.Requests.Database.Operation;
using UnityiOSCloud.CloudKit.Requests.Database.Query;
using UnityiOSCloud.CloudKit.Requests.Database.Record;
using UnityiOSCloud.CloudKit.Requests.Database.Zone;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Container.Database
{
    [Serializable]
    public class CKSDatabase
    {
        [SerializeField]
        private string _containerId;
        
        [SerializeField]
        private CKSDatabaseScope _scope;

        internal CKSDatabase(string containerId, CKSDatabaseScope scope)
        {
            _containerId = containerId;
            _scope = scope;
        }
        
        #region Query Interface
        public void PerformQuery(CKSQuery query, Action<CKSResult> callback)
        {
            CKSPerformQuery request = new CKSPerformQuery(this, query, callback);
            CloudKitStorageBridge.PerformQuery(request);
        }
        #endregion
        
        #region Records Interface
        public void SaveRecord(CKSRecord record, Action<CKSResult> callback)
        {
            CKSSaveRecord request = new CKSSaveRecord(this, record, callback);
            CloudKitStorageBridge.SaveRecord(request);
        }
        
        public void DeleteRecord(CKSRecordID recordId, Action<CKSResult> callback)
        {
            CKSDeleteRecord request = new CKSDeleteRecord(this, recordId, callback);
            CloudKitStorageBridge.DeleteRecord(request);
        }
        
        public void FetchRecord(CKSRecordID recordId, Action<CKSResult> callback)
        {
            CKSFetchRecord request = new CKSFetchRecord(this, recordId, callback);
            CloudKitStorageBridge.FetchRecord(request);
        }
        #endregion
        
        #region Zones Interface
        public void SaveRecordZone(CKSZone zone, Action<CKSResult> callback)
        {
            CKSSaveRecordZone request = new CKSSaveRecordZone(this, zone, callback);
            CloudKitStorageBridge.SaveRecordZone(request);
        }
        
        public void DeleteRecordZone(string zoneName, Action<CKSResult> callback)
        {
            CKSDeleteRecordZone request = new CKSDeleteRecordZone(this, zoneName, callback);
            CloudKitStorageBridge.DeleteRecordZone(request);
        }
        
        public void FetchRecordZone(string zoneName, Action<CKSResult> callback)
        {
            CKSFetchRecordZone request = new CKSFetchRecordZone(this, zoneName, callback);
            CloudKitStorageBridge.FetchRecordZone(request);
        }
        
        public void FetchAllRecordZones(Action<CKSResult> callback)
        {
            CKSFetchAllRecordZones request = new CKSFetchAllRecordZones(this, callback);
            CloudKitStorageBridge.FetchAllRecordZones(request);
        }
        #endregion
        
        #region Operations Interface
        public void FetchMultipleRecords(CKSRecordID[] recordIds, Action<CKSResult> callback)
        {
            CKSFetchMultipleRecords request = new CKSFetchMultipleRecords(this, recordIds, callback);
            CloudKitStorageBridge.FetchMultipleRecords(request);
        }
        
        public void FetchMultipleRecords(List<CKSRecordID> recordIds, Action<CKSResult> callback)
        {
            FetchMultipleRecords(recordIds.ToArray(), callback);
        }
        
        public void FetchMultipleRecordZones(string[] zoneIds, Action<CKSResult> callback)
        {
            CKSFetchMultipleRecordZones request = new CKSFetchMultipleRecordZones(this, zoneIds, callback);
            CloudKitStorageBridge.FetchMultipleRecordZones(request);
        }
        
        public void FetchMultipleRecordZones(List<string> zoneIds, Action<CKSResult> callback)
        {
            FetchMultipleRecordZones(zoneIds.ToArray(), callback);
        }
        
        public void ModifyRecords(CKSModifyRecordsParams modifyParams, Action<CKSResult> callback)
        {
            CKSModifyRecords request = new CKSModifyRecords(this, modifyParams, callback);
            CloudKitStorageBridge.ModifyRecords(request);
        }
        
        public void ModifyRecordZones(CKSModifyRecordZonesParams modifyParams, Action<CKSResult> callback)
        {
            CKSModifyRecordZones request = new CKSModifyRecordZones(this, modifyParams, callback);
            CloudKitStorageBridge.ModifyRecordZones(request);
        }
        
        #endregion

        public CKSDatabaseScope Scope
        {
            get { return _scope; }
        }
    }
}