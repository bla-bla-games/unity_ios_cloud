﻿using System;
using UnityEngine;

namespace UnityiOSCloud.CloudKit.Container.Database
{
	[Serializable]
	public class CKSQuery
	{
		[SerializeField]
		private string _zoneName;

		[SerializeField]
		private string _recordType;
		
		[SerializeField]
		private string _predicate;

		public CKSQuery(string recordType, string zoneName, string predicate)
		{
			_recordType = recordType;
			_zoneName = zoneName;
			_predicate = predicate;
		}
		
		public CKSQuery(string recordType, string predicate) : this(recordType, CKSZone.DEFAULT_ZONE_NAME, predicate)
		{
		}

		public string ZoneName
		{
			get { return _zoneName; }
			set { _zoneName = value; }
		}

		public string RecordType
		{
			get { return _recordType; }
			set { _recordType = value; }
		}

		public string Predicate
		{
			get { return _predicate; }
			set { _predicate = value; }
		}
	}
}