﻿using System;
using UnityiOSCloud.CloudKit.Container.Database;
using UnityiOSCloud.CloudKit.Requests.Container.Account;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Container
{
    public class CKSContainer
    {
        internal const string DEFAULT_CONTAINER_ID = "-1";
        
        public string Id { get; private set; }
        
        public CKSDatabase PublicDatabase { get; private set; }
        public CKSDatabase PrivateDatabase { get; private set; }

        internal CKSContainer(string id)
        {
            Id = id;
            InitDatabases();
        }

        private void InitDatabases()
        {
            PublicDatabase = new CKSDatabase(Id, CKSDatabaseScope.Public);
            PrivateDatabase = new CKSDatabase(Id, CKSDatabaseScope.Private);
        }

        public void CheckAccountStatus(Action<CKSResult> callback)
        {
            CKSCheckAccountStatus request = new CKSCheckAccountStatus(Id, callback);
            CloudKitStorageBridge.CheckAccountStatus(request);
        }

        public bool IsDefault
        {
            get { return Id == DEFAULT_CONTAINER_ID; }
        }
    }
}