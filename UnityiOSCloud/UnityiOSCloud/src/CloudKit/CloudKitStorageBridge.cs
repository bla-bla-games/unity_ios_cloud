﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using AOT;
using UnityEngine;
using UnityiOSCloud.CloudKit.Requests;
using UnityiOSCloud.CloudKit.Result;
using UnityiOSCloud.CloudKit.Result.Container;
using UnityiOSCloud.CloudKit.Result.Database.Operation;
using UnityiOSCloud.CloudKit.Result.Database.Query;
using UnityiOSCloud.CloudKit.Result.Database.Record;
using UnityiOSCloud.CloudKit.Result.Database.Zone;

namespace UnityiOSCloud.CloudKit
{
	public class CloudKitStorageBridge
	{
		private class NativeAPI
		{
			[DllImport("__Internal")]
			internal static extern void _native_activate(
				Callbacks.CKSAccountStatusHandler accountStatusCB,
				Callbacks.CKSPerformQueryHandler performQueryCB,
				Callbacks.CKSSaveRecordHandler saveRecordCB,
				Callbacks.CKSDeleteRecordHandler deleteRecordCB,
				Callbacks.CKSFetchRecordHandler fetchRecordCB,
				Callbacks.CKSSaveRecordZoneHandler saveRecordZoneCB,
				Callbacks.CKSDeleteRecordZoneHandler deleteRecordZoneCB,
				Callbacks.CKSFetchRecordZoneHandler fetchRecordZoneCB,
				Callbacks.CKSFetchAllRecordZonesHandler fetchAllRecordZonesCB,
				Callbacks.CKSFetchMultipleRecordsHandler fetchMultipleRecordsCB,
				Callbacks.CKSFetchMultipleRecordZonesHandler fetchMultipleRecordZonesCB,
				Callbacks.CKSModifyRecordsHandler modifyRecordsCB,
				Callbacks.CKSModifyRecordZonesHandler modifyRecorZonesCB);

			[DllImport("__Internal")]
			internal static extern void _native_deactivate();
			
			#region Account Status

			[DllImport("__Internal")]
			internal static extern void _native_accountStatus(string requestData);

			#endregion

			#region Query Interface

			[DllImport("__Internal")]
			internal static extern void _native_performQuery(string requestData);

			#endregion

			#region Records Interface

			[DllImport("__Internal")]
			internal static extern void _native_saveRecord(string requestData);

			[DllImport("__Internal")]
			internal static extern void _native_deleteRecord(string requestData);

			[DllImport("__Internal")]
			internal static extern void _native_fetchRecord(string requestData);

			#endregion

			#region Record Zones Interface

			[DllImport("__Internal")]
			internal static extern void _native_saveRecordZone(string requestData);

			[DllImport("__Internal")]
			internal static extern void _native_deleteRecordZone(string requestData);

			[DllImport("__Internal")]
			internal static extern void _native_fetchRecordZone(string requestData);

			[DllImport("__Internal")]
			internal static extern void _native_fetchAllRecordZones(string requestData);

			#endregion

			#region Operations Interface

			[DllImport("__Internal")]
			internal static extern void _native_fetchMultipleRecords(string requestData);
			
			[DllImport("__Internal")]
			internal static extern void _native_fetchMultipleRecordZones(string requestData);
			
			[DllImport("__Internal")]
			internal static extern void _native_modifyRecords(string requestData);
			
			[DllImport("__Internal")]
			internal static extern void _native_modifyRecordZones(string requestData);

			#endregion
		}

		private class Callbacks
		{
			#region Delegates
			internal delegate void CKSAccountStatusHandler(string requestId, string responseData);
			
			internal delegate void CKSPerformQueryHandler(string requestId, string responseData);
			
			internal delegate void CKSSaveRecordHandler(string requestId, string responseData);
			internal delegate void CKSDeleteRecordHandler(string requestId, string responseData);
			internal delegate void CKSFetchRecordHandler(string requestId, string responseData);
			
			internal delegate void CKSSaveRecordZoneHandler(string requestId, string responseData);
			internal delegate void CKSDeleteRecordZoneHandler(string requestId, string responseData);
			internal delegate void CKSFetchRecordZoneHandler(string requestId, string responseData);
			internal delegate void CKSFetchAllRecordZonesHandler(string requestId, string responseData);
			
			internal delegate void CKSFetchMultipleRecordsHandler(string requestId, string responseData);
			internal delegate void CKSFetchMultipleRecordZonesHandler(string requestId, string responseData);
			internal delegate void CKSModifyRecordsHandler(string requestId, string responseData);
			internal delegate void CKSModifyRecordZonesHandler(string requestId, string responseData);
			#endregion

			private static void RequestCallback<T>(string requestId, string responseData) where T : CKSResult
			{
				if (!_activeRequests.ContainsKey(requestId))
				{
					throw new ArgumentException(string.Format("Can't find request '{0}'!", requestId));
				}
				
				CKSRequest request = _activeRequests[requestId];
				_activeRequests.Remove(requestId);
				request.Response<T>(responseData);
			}

			#region Native Callbacks
			[MonoPInvokeCallback(typeof(CKSAccountStatusHandler))]
			internal static void HandleAccountStatus(string requestId, string responseData)
			{
				RequestCallback<CKSAccountStatusResult>(requestId, responseData);
			}
			
			[MonoPInvokeCallback(typeof(CKSPerformQueryHandler))]
			internal static void HandlePerformQuery(string requestId, string responseData)
			{
				RequestCallback<CKSPerformQueryResult>(requestId, responseData);
			}
			
			[MonoPInvokeCallback(typeof(CKSSaveRecordHandler))]
			internal static void HandleSaveRecord(string requestId, string responseData)
			{
				RequestCallback<CKSSaveRecordResult>(requestId, responseData);
			}

			[MonoPInvokeCallback(typeof(CKSDeleteRecordHandler))]
			internal static void HandleDeleteRecord(string requestId, string responseData)
			{
				RequestCallback<CKSDeleteRecordResult>(requestId, responseData);
			}

			[MonoPInvokeCallback(typeof(CKSFetchRecordHandler))]
			internal static void HandleFetchRecord(string requestId, string responseData)
			{
				RequestCallback<CKSFetchRecordResult>(requestId, responseData);
			}

			[MonoPInvokeCallback(typeof(CKSSaveRecordZoneHandler))]
			internal static void HandleSaveRecordZone(string requestId, string responseData)
			{
				RequestCallback<CKSSaveRecordZoneResult>(requestId, responseData);
			}

			[MonoPInvokeCallback(typeof(CKSDeleteRecordZoneHandler))]
			internal static void HandleDeleteRecordZone(string requestId, string responseData)
			{
				RequestCallback<CKSDeleteRecordZoneResult>(requestId, responseData);
			}

			[MonoPInvokeCallback(typeof(CKSFetchRecordZoneHandler))]
			internal static void HandleFetchRecordZone(string requestId, string responseData)
			{
				RequestCallback<CKSFetchRecordZoneResult>(requestId, responseData);
			}
			
			[MonoPInvokeCallback(typeof(CKSFetchAllRecordZonesHandler))]
			internal static void HandleFetchAllRecordZones(string requestId, string responseData)
			{
				RequestCallback<CKSFetchAllRecordZonesResult>(requestId, responseData);
			}
			
			[MonoPInvokeCallback(typeof(CKSFetchMultipleRecordsHandler))]
			internal static void HandleFetchMultipleRecords(string requestId, string responseData)
			{
				RequestCallback<CKSFetchMultipleRecordsResult>(requestId, responseData);
			}
			
			[MonoPInvokeCallback(typeof(CKSFetchMultipleRecordZonesHandler))]
			internal static void HandleFetchMultipleRecordZones(string requestId, string responseData)
			{
				RequestCallback<CKSFetchMultipleRecordZonesResult>(requestId, responseData);
			}
			
			[MonoPInvokeCallback(typeof(CKSModifyRecordsHandler))]
			internal static void HandleModifyRecords(string requestId, string responseData)
			{
				RequestCallback<CKSModifyRecordsResult>(requestId, responseData);
			}
			
			[MonoPInvokeCallback(typeof(CKSModifyRecordZonesHandler))]
			internal static void HandleModifyRecordZones(string requestId, string responseData)
			{
				RequestCallback<CKSModifyRecordZonesResult>(requestId, responseData);
			}
			#endregion
		}
		
		private static Dictionary<string, CKSRequest> _activeRequests;

		static CloudKitStorageBridge()
		{
			_activeRequests = new Dictionary<string, CKSRequest>();
		}
		
		internal static void CacheRequest(CKSRequest request)
		{	
			if (_activeRequests.ContainsKey(request.Id))
			{
				throw new ArgumentException(string.Format("Request '{0}' is already added!", request.Id));
			}

			_activeRequests.Add(request.Id, request);
		}
		
		internal static void Activate()
		{
			NativeAPI._native_activate(
				Callbacks.HandleAccountStatus,
				Callbacks.HandlePerformQuery,
				Callbacks.HandleSaveRecord,
				Callbacks.HandleDeleteRecord,
				Callbacks.HandleFetchRecord,
				Callbacks.HandleSaveRecordZone,
				Callbacks.HandleDeleteRecordZone,
				Callbacks.HandleFetchRecordZone,
				Callbacks.HandleFetchAllRecordZones,
				Callbacks.HandleFetchMultipleRecords,
				Callbacks.HandleFetchMultipleRecordZones,
				Callbacks.HandleModifyRecords,
				Callbacks.HandleModifyRecordZones);
		}
		
		internal static void Deactivate()
		{
			NativeAPI._native_deactivate();
		}
		
		#region Managed Account Status Interface

		internal static void CheckAccountStatus(CKSRequest request)
		{
			CacheRequest(request);
			
			string requestData = JsonUtility.ToJson(request);
			NativeAPI._native_accountStatus(requestData);
		}

		#endregion

		#region Managed Query Interface

		internal static void PerformQuery(CKSRequest request)
		{
			CacheRequest(request);
			
			string requestData = JsonUtility.ToJson(request);
			NativeAPI._native_performQuery(requestData);
		}

		#endregion

		#region Records Interface

		internal static void SaveRecord(CKSRequest request)
		{
			CacheRequest(request);
			
			string requestData = JsonUtility.ToJson(request);
			NativeAPI._native_saveRecord(requestData);
		}

		internal static void DeleteRecord(CKSRequest request)
		{
			CacheRequest(request);
			
			string requestData = JsonUtility.ToJson(request);
			NativeAPI._native_deleteRecord(requestData);
		}

		internal static void FetchRecord(CKSRequest request)
		{
			CacheRequest(request);
			
			string requestData = JsonUtility.ToJson(request);
			NativeAPI._native_fetchRecord(requestData);
		}

		#endregion

		#region Record Zones Interface

		internal static void SaveRecordZone(CKSRequest request)
		{
			CacheRequest(request);
			
			string requestData = JsonUtility.ToJson(request);
			NativeAPI._native_saveRecordZone(requestData);
		}

		internal static void DeleteRecordZone(CKSRequest request)
		{
			CacheRequest(request);
			
			string requestData = JsonUtility.ToJson(request);
			NativeAPI._native_deleteRecordZone(requestData);
		}

		internal static void FetchRecordZone(CKSRequest request)
		{
			CacheRequest(request);
			
			string requestData = JsonUtility.ToJson(request);
			NativeAPI._native_fetchRecordZone(requestData);
		}

		internal static void FetchAllRecordZones(CKSRequest request)
		{
			CacheRequest(request);
			
			string requestData = JsonUtility.ToJson(request);
			NativeAPI._native_fetchAllRecordZones(requestData);
		}

		#endregion

		#region Operations Interface

		internal static void FetchMultipleRecords(CKSRequest request)
		{
			CacheRequest(request);
			
			string requestData = JsonUtility.ToJson(request);
			NativeAPI._native_fetchMultipleRecords(requestData);
		}
		
		internal static void FetchMultipleRecordZones(CKSRequest request)
		{
			CacheRequest(request);
			
			string requestData = JsonUtility.ToJson(request);
			NativeAPI._native_fetchMultipleRecordZones(requestData);
		}
		
		internal static void ModifyRecords(CKSRequest request)
		{
			CacheRequest(request);
			
			string requestData = JsonUtility.ToJson(request);
			NativeAPI._native_modifyRecords(requestData);
		}
		
		internal static void ModifyRecordZones(CKSRequest request)
		{
			CacheRequest(request);
			
			string requestData = JsonUtility.ToJson(request);
			NativeAPI._native_modifyRecordZones(requestData);
		}

		#endregion
	}
}