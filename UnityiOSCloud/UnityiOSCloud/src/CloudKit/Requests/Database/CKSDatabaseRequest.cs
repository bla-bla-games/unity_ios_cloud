﻿using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Requests.Database
{
	[Serializable]
	public class CKSDatabaseRequest : CKSRequest
	{
		[SerializeField]
		private CKSDatabase _database;
		
		public CKSDatabaseRequest(CKSDatabase database, Action<CKSResult> callback) : base(callback)
		{
			_database = database;
		}
	}
}