using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Requests.Database.Record
{
	[Serializable]
	public class CKSSaveRecord : CKSDatabaseRequest
	{
		[SerializeField]
		public CKSRecord _record;
		
		internal CKSSaveRecord(CKSDatabase database, CKSRecord record, Action<CKSResult> callback) : base(database, callback)
		{
			_record = record;
		}
	}
}