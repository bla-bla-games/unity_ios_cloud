using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Requests.Database.Record
{
	public class CKSFetchRecord : CKSDatabaseRequest
	{
		[SerializeField]
		private CKSRecordID _recordId;
		
		internal CKSFetchRecord(CKSDatabase database, CKSRecordID recordId, Action<CKSResult> callback) : base(database, callback)
		{
			_recordId = recordId;
		}
	}
}