using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Requests.Database.Zone
{
	[Serializable]
	public class CKSSaveRecordZone : CKSDatabaseRequest
	{
		[SerializeField]
		public CKSZone _zone;
		
		internal CKSSaveRecordZone(CKSDatabase database, CKSZone zone, Action<CKSResult> callback) : base(database, callback)
		{
			_zone = zone;
		}
	}
}