using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Requests.Database.Zone
{
	[Serializable]
	public class CKSDeleteRecordZone : CKSDatabaseRequest
	{
		[SerializeField]
		public string _zoneName;
		
		internal CKSDeleteRecordZone(CKSDatabase database, string zoneName, Action<CKSResult> callback) : base(database, callback)
		{
			_zoneName = zoneName;
		}
	}
}