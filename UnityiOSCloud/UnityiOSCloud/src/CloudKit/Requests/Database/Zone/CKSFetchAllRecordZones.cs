using System;
using UnityiOSCloud.CloudKit.Container.Database;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Requests.Database.Zone
{
	[Serializable]
	public class CKSFetchAllRecordZones : CKSDatabaseRequest
	{
		internal CKSFetchAllRecordZones(CKSDatabase database, Action<CKSResult> callback) : base(database, callback)
		{
		}
	}
}