using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Requests.Database.Query
{
	[Serializable]
	public class CKSPerformQuery : CKSDatabaseRequest
	{
		[SerializeField]
		private CKSQuery _query;
		
		internal CKSPerformQuery(CKSDatabase database, CKSQuery query, Action<CKSResult> callback) : base(database, callback)
		{
			_query = query;
		}
	}
}