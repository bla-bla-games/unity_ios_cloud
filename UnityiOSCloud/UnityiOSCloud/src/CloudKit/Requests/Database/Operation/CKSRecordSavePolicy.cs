namespace UnityiOSCloud.CloudKit.Requests.Database.Operation
{
	public enum CKSRecordSavePolicy
	{
		CKRecordSaveIfServerRecordUnchanged = 0,
		CKRecordSaveChangedKeys,
		CKRecordSaveAllKeys
	}
}