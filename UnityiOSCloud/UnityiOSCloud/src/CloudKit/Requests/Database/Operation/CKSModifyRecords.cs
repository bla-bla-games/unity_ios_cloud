using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Requests.Database.Operation
{
	[Serializable]
	public class CKSModifyRecords : CKSDatabaseRequest
	{
		[SerializeField]
		private CKSRecord[] _recordsToSave;
		
		[SerializeField]
		private CKSRecordID[] _recordIdsToDelete;
		
		[SerializeField]
		private CKSRecordSavePolicy _savePolicy;
		
		[SerializeField]
		private bool _isAtomic;
		
		public CKSModifyRecords(CKSDatabase database, CKSModifyRecordsParams modifyParams, Action<CKSResult> callback) : base(database, callback)
		{
			_recordsToSave = modifyParams.RecordsToSave;
			_recordIdsToDelete = modifyParams.RecordIDsToDelete;
			_savePolicy = modifyParams.SavePolicy;
			_isAtomic = modifyParams.IsAtomic;
		}
	}
}