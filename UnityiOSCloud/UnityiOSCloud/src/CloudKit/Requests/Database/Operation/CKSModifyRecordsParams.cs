using UnityiOSCloud.CloudKit.Container.Database;

namespace UnityiOSCloud.CloudKit.Requests.Database.Operation
{
	public class CKSModifyRecordsParams
	{
		public CKSRecord[] RecordsToSave { get; private set; }
		public CKSRecordID[] RecordIDsToDelete { get; private set; }
		public CKSRecordSavePolicy SavePolicy  { get; set; }
		public bool IsAtomic { get; set; }

		public CKSModifyRecordsParams(CKSRecord[] recordsToSave, CKSRecordID[] recordIDsToDelete)
		{
			RecordsToSave = recordsToSave ?? new CKSRecord[]{};
			RecordIDsToDelete = recordIDsToDelete ?? new CKSRecordID[]{};
			SavePolicy = CKSRecordSavePolicy.CKRecordSaveIfServerRecordUnchanged;
			IsAtomic = true;
		}
		
		public CKSModifyRecordsParams() : this(new CKSRecord[]{}, new CKSRecordID[]{})
		{
		}
	}
}