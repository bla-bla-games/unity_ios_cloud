using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Requests.Database.Operation
{
	[Serializable]
	public class CKSFetchMultipleRecords : CKSDatabaseRequest
	{
		[SerializeField]
		private CKSRecordID[] _recordIds;
		
		internal CKSFetchMultipleRecords(CKSDatabase database, CKSRecordID[] recordIds, Action<CKSResult> callback) : base(database, callback)
		{
			_recordIds = recordIds;
		}
	}
}