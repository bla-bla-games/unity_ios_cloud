using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Requests.Database.Operation
{
	[Serializable]
	public class CKSModifyRecordZones : CKSDatabaseRequest
	{
		[SerializeField]
		private CKSZone[] _zonesToSave;
		
		[SerializeField]
		private string[] _zoneNamesToDelete;
		
		public CKSModifyRecordZones(CKSDatabase database, CKSModifyRecordZonesParams modifyParams, Action<CKSResult> callback) : base(database, callback)
		{
			_zonesToSave = modifyParams.ZonesToSave;
			_zoneNamesToDelete = modifyParams.ZoneNamesToDelete;
		}
	}
}