using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Requests.Database.Operation
{
	[Serializable]
	public class CKSFetchMultipleRecordZones : CKSDatabaseRequest
	{
		[SerializeField]
		private string[] _zoneIds;
		
		internal CKSFetchMultipleRecordZones(CKSDatabase database, string[] zoneIds, Action<CKSResult> callback) : base(database, callback)
		{
			_zoneIds = zoneIds;
		}
	}
}