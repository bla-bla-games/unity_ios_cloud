using UnityiOSCloud.CloudKit.Container.Database;

namespace UnityiOSCloud.CloudKit.Requests.Database.Operation
{
	public class CKSModifyRecordZonesParams
	{
		public CKSZone[] ZonesToSave { get; private set; }
		public string[] ZoneNamesToDelete { get; private set; }

		public CKSModifyRecordZonesParams(CKSZone[] zonesToSave, string[] zoneNamesToDelete)
		{
			ZonesToSave = zonesToSave ?? new CKSZone[]{};
			ZoneNamesToDelete = zoneNamesToDelete ?? new string[]{};
		}
		
		public CKSModifyRecordZonesParams() : this(new CKSZone[]{}, new string[]{})
		{
		}
	}
}