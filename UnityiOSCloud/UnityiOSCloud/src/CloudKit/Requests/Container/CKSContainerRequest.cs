﻿using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Requests.Container
{
	[Serializable]
	public class CKSContainerRequest : CKSRequest
	{
		[SerializeField]
		private string _containerId;
		
		public CKSContainerRequest(string containerId, Action<CKSResult> callback) : base(callback)
		{
			_containerId = containerId;
		}
	}
}