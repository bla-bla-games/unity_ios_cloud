using System;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Requests.Container.Account
{
	[Serializable]
	public class CKSCheckAccountStatus : CKSContainerRequest
	{
		public CKSCheckAccountStatus(string containerId, Action<CKSResult> callback) : base(containerId, callback)
		{
		}
	}
}