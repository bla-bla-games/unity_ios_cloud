using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Result;

namespace UnityiOSCloud.CloudKit.Requests
{
	[Serializable]
	public class CKSRequest
	{
		private static long CURRENT_REQUEST_ID_INDEX = 0;
		
		[SerializeField]
		private string _id;
		
		private Action<CKSResult> _callback;

		internal CKSRequest(Action<CKSResult> callback)
		{
			_id = GenerateId();
			_callback = callback;
		}

		internal virtual void Response<T>(string responseData) where T : CKSResult
		{
			if (_callback == null)
			{
				return;
			}
			
			T result = JsonUtility.FromJson<T>(responseData);
			_callback(result);
		}

		public string Id
		{
			get { return _id; }
		}

		private static string GenerateId()
		{
			CURRENT_REQUEST_ID_INDEX += 1;
			return CURRENT_REQUEST_ID_INDEX.ToString();
		}
	}
}