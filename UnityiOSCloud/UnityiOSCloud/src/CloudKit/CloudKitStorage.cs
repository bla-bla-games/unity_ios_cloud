﻿using System;
using System.Collections.Generic;
using UnityiOSCloud.CloudKit.Container;

namespace UnityiOSCloud.CloudKit
{
    public class CloudKitStorage
    {
        private readonly Dictionary<string, CKSContainer> _registeredContainers;

        internal CloudKitStorage()
        {
            _registeredContainers = new Dictionary<string, CKSContainer>();
            Initialize();
        }

        private void Initialize()
        {
            RegisterContainer(CKSContainer.DEFAULT_CONTAINER_ID);
        }

        public void Activate()
        {
            CloudKitStorageBridge.Activate();
        }
        
        public void Deactivate()
        {
            CloudKitStorageBridge.Deactivate();
        }

        public CKSContainer RegisterContainer(string containerId)
        {
            if (_registeredContainers.ContainsKey(containerId))
            {
                throw new ArgumentException(string.Format("CloudKit container '{0}' is already registered!", containerId));
            }
            
            CKSContainer result = new CKSContainer(containerId);
            _registeredContainers.Add(containerId, result);

            return result;
        }

        public bool IsContainerExist(string containerId)
        {
            return _registeredContainers.ContainsKey(containerId);
        }
        
        public CKSContainer GetContainer(string containerId)
        {
            if (!_registeredContainers.ContainsKey(containerId))
            {
                throw new ArgumentException(string.Format("CloudKit container '{0}' is not registered!", containerId));
            }
            
            return _registeredContainers[containerId];
        }
        
        public CKSContainer GetDefaultContainer()
        {
            return GetContainer(CKSContainer.DEFAULT_CONTAINER_ID);
        }
    }
}