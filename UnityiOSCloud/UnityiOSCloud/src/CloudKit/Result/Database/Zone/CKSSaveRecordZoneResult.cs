using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;

namespace UnityiOSCloud.CloudKit.Result.Database.Zone
{
	[Serializable]
	public class CKSSaveRecordZoneResult : CKSResult
	{
		[SerializeField]
		private CKSZone _zone;
		
		public CKSZone Zone
		{
			get { return _zone; }
		}
	}
}