using System;
using UnityEngine;

namespace UnityiOSCloud.CloudKit.Result.Database.Zone
{
	[Serializable]
	public class CKSDeleteRecordZoneResult : CKSResult
	{
		[SerializeField]
		private string _zoneName;
		
		public string ZoneName
		{
			get { return _zoneName; }
		}
	}
}