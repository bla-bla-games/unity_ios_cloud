using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;

namespace UnityiOSCloud.CloudKit.Result.Database.Zone
{
	[Serializable]
	public class CKSFetchAllRecordZonesResult : CKSResult
	{
		[SerializeField]
		private CKSZone[] _zones;
		
		public CKSZone[] Zones
		{
			get { return _zones; }
		}
	}
}