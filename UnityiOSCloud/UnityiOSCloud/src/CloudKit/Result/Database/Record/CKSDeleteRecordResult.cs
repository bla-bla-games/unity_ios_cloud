using System;
using UnityEngine;

namespace UnityiOSCloud.CloudKit.Result.Database.Record
{
	[Serializable]
	public class CKSDeleteRecordResult : CKSResult
	{
		[SerializeField]
		private string _recordId;
		
		[SerializeField]
		private string _zoneName;
		
		public string RecordId
		{
			get { return _recordId; }
		}
		
		public string ZoneName
		{
			get { return _zoneName; }
		}
	}
}