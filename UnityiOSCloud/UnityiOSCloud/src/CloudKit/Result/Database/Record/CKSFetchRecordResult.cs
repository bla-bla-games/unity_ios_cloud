using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;

namespace UnityiOSCloud.CloudKit.Result.Database.Record
{
	public class CKSFetchRecordResult : CKSResult
	{
		[SerializeField]
		private CKSRecord _record;
		
		public CKSRecord Record
		{
			get { return _record; }
		}
	}
}