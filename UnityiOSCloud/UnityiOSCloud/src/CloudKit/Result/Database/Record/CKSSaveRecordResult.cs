using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;

namespace UnityiOSCloud.CloudKit.Result.Database.Record
{
	[Serializable]
	public class CKSSaveRecordResult : CKSResult
	{
		[SerializeField]
		private CKSRecord _record;
		
		public CKSRecord Record
		{
			get { return _record; }
		}
	}
}