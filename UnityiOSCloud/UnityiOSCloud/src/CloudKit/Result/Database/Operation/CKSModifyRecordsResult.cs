using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;
using UnityiOSCloud.CloudKit.Error;

namespace UnityiOSCloud.CloudKit.Result.Database.Operation
{
	[Serializable]
	public class CKSModifyRecordsResult : CKSResult
	{
		[SerializeField]
		private CKSRecord[] _savedRecords;
		
		[SerializeField]
		private CKSRecordID[] _deletedRecordIDs;
		
		[SerializeField]
		private CKSRecordError[] _recordErrors;
		
		public CKSRecord[] SavedRecords
		{
			get { return _savedRecords; }
		}
		
		public CKSRecordID[] DeletedRecordIDs
		{
			get { return _deletedRecordIDs; }
		}
		
		public CKSRecordError[] RecordErrors
		{
			get { return _recordErrors; }
		}
	}
}