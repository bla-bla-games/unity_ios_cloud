using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;

namespace UnityiOSCloud.CloudKit.Result.Database.Operation
{
	[Serializable]
	public class CKSFetchMultipleRecordZonesResult : CKSResult
	{
		[SerializeField]
		private CKSZone[] _zones;
		
		public CKSZone[] Zones
		{
			get { return _zones; }
		}
	}
}