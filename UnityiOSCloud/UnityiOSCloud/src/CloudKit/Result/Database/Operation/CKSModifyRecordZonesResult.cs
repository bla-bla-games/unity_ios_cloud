using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;

namespace UnityiOSCloud.CloudKit.Result.Database.Operation
{
	[Serializable]
	public class CKSModifyRecordZonesResult : CKSResult
	{
		[SerializeField]
		private CKSZone[] _savedZones;
		
		[SerializeField]
		private string[] _deletedZoneNames;
		
		public CKSZone[] SavedZones
		{
			get { return _savedZones; }
		}
		
		public string[] DeletedZoneNames
		{
			get { return _deletedZoneNames; }
		}
	}
}