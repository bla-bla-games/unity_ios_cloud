using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;
using UnityiOSCloud.CloudKit.Error;

namespace UnityiOSCloud.CloudKit.Result.Database.Operation
{
	[Serializable]
	public class CKSFetchMultipleRecordsResult : CKSResult
	{
		[SerializeField]
		private CKSRecord[] _records;
		
		[SerializeField]
		private CKSRecordError[] _recordErrors;
		
		public CKSRecord[] Records
		{
			get { return _records; }
		}
		
		public CKSRecordError[] RecordErrors
		{
			get { return _recordErrors; }
		}
	}
}