using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;

namespace UnityiOSCloud.CloudKit.Result.Database.Query
{
	[Serializable]
	public class CKSPerformQueryResult : CKSResult
	{
		[SerializeField]
		private CKSRecord[] _records;
		
		public CKSRecord[] Records
		{
			get { return _records; }
		}
	}
}