namespace UnityiOSCloud.CloudKit.Result.Container
{
	public enum CKSAccountStatus
	{
		CKAccountStatusCouldNotDetermine = 0,
		CKAccountStatusAvailable,
		CKAccountStatusRestricted,
		CKAccountStatusNoAccount
	}
}