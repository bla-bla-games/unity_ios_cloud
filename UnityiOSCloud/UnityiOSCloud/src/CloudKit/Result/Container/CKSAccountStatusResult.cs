using System;
using UnityEngine;

namespace UnityiOSCloud.CloudKit.Result.Container
{
	[Serializable]
	public class CKSAccountStatusResult : CKSResult
	{
		[SerializeField]
		private CKSAccountStatus _status;

		public CKSAccountStatus Status
		{
			get { return _status; }
		}
	}
}