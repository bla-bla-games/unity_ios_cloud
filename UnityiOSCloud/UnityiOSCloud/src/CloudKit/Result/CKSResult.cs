using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Error;

namespace UnityiOSCloud.CloudKit.Result
{
	[Serializable]
	public class CKSResult
	{
		[SerializeField]
		private bool _isSuccess;
		
		[SerializeField]
		private CKSError _error;

		public CKSError Error
		{
			get { return _error; }
		}
		
		public bool IsSuccess
		{
			get { return _isSuccess; }
		}
	}
}