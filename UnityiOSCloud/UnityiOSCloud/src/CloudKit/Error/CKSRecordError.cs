using System;
using UnityEngine;
using UnityiOSCloud.CloudKit.Container.Database;

namespace UnityiOSCloud.CloudKit.Error
{
	[Serializable]
	public class CKSRecordError : CKSError
	{
		[SerializeField]
		private CKSRecordID _recordId;

		public CKSRecordID RecordId
		{
			get { return _recordId; }
		}
		
		public override string ToString()
		{
			return string.Format("[CKSRecordError({0})][{1}]: {2}", Code, RecordId, Message);
		}
	}
}