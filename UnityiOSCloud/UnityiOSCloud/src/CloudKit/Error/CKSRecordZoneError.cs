using System;
using UnityEngine;

namespace UnityiOSCloud.CloudKit.Error
{
	[Serializable]
	public class CKSRecordZoneError : CKSError
	{
		[SerializeField]
		private string _zoneName;

		public string ZoneName
		{
			get { return _zoneName; }
		}
		
		public override string ToString()
		{
			return string.Format("[CKSRecordZoneError({0})][Id:{1}]: {2}", Code, ZoneName, Message);
		}
	}
}