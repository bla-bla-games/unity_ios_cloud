using System;
using UnityEngine;

namespace UnityiOSCloud.CloudKit.Error
{
	[Serializable]
	public class CKSError
	{
		[SerializeField]
		private int _code;
		
		[SerializeField]
		private string _message;
		
		public int Code
		{
			get { return _code; }
		}
		
		public string Message
		{
			get { return _message; }
		}
		
		public override string ToString()
		{
			return string.Format("[CKSError({0})]: {1}", Code, Message);
		}
	}
}