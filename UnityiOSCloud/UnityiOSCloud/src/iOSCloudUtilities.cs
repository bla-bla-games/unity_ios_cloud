﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace UnityiOSCloud
{
    public class iOSCloudUtilities
    {
        public static int ParseInt(string value)
        {
            int result;
            int.TryParse(value, out result);
            return result;
        }

        public static long ParseLong(string value)
        {
            long result;
            long.TryParse(value, out result);
            return result;
        }

        public static float ParseFloat(string value)
        {
            float result;
            float.TryParse(value, out result);
            return result;
        }

        public static double ParseDouble(string value)
        {
            double result;
            double.TryParse(value, out result);
            return result;
        }

        public static bool ParseBool(string value)
        {
            bool result;
            bool.TryParse(value, out result);
            return result;
        }

        public static Color ParseColor(string value)
        {
            Color result;
            ColorUtility.TryParseHtmlString("#" + value, out result);
            return result;
        }

        public static byte[] ParseData(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            return Convert.FromBase64String(value);
        }
    }
}