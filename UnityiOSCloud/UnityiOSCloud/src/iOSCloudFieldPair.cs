﻿//
//  ChangePair.cs
//
//  Product: UnityiOSCloud
//  Author: Sergey Smirnov
//  Support: d.duck.dev@gmail.com
//

using System;
using UnityEngine;

namespace UnityiOSCloud
{
    [Serializable]
    public class iOSCloudFieldPair
    {
        [SerializeField]
        private string _key;

        [SerializeField]
        private string _value;

        internal iOSCloudFieldPair(string key, string value)
        {
            _key = key;
            _value = value;
        }
        
        public void Set(string value)
        {
            _value = value;
        }

        /// <summary>
        /// A key in the key-value store.
        /// </summary>
        public string Key
        {
            get { return _key; }
        }

        /// <summary>
        /// Returns raw representation of value.
        /// </summary>
        public string Value
        {
            get { return _value; }
        }

        /// <summary>
        /// Returns int representation of value.
        /// </summary>
        public int IntValue
        {
            get { return iOSCloudUtilities.ParseInt(_value); }
        }

        /// <summary>
        /// Returns long representation of value.
        /// </summary>
        public long LongValue
        {
            get { return iOSCloudUtilities.ParseLong(_value); }
        }

        /// <summary>
        /// Returns float representation of value.
        /// </summary>
        public float FloatValue
        {
            get { return iOSCloudUtilities.ParseFloat(_value); }
        }

        /// <summary>
        /// Returns double representation of value.
        /// </summary>
        public double DoubleValue
        {
            get { return iOSCloudUtilities.ParseDouble(_value); }
        }

        /// <summary>
        /// Returns bool representation of value.
        /// </summary>
        public bool BoolValue
        {
            get { return iOSCloudUtilities.ParseBool(_value); }
        }

        /// <summary>
        /// Returns Color representation of value.
        /// </summary>
        public Color ColorValue
        {
            get { return iOSCloudUtilities.ParseColor(_value); }
        }

        /// <summary>
        /// Returns Color32 representation of value.
        /// </summary>
        public Color32 Color32Value
        {
            get { return iOSCloudUtilities.ParseColor(_value); }
        }

        /// <summary>
        /// Returns byte[] representation of value.
        /// </summary>
        public byte[] DataValue
        {
            get { return iOSCloudUtilities.ParseData(_value); }
        }
    }
}