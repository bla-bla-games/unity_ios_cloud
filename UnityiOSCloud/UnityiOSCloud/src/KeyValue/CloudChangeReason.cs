﻿//
//  CloudChangeReason.cs
//
//  Product: UnityiOSCloud
//  Author: Sergey Smirnov
//  Support: d.duck.dev@gmail.com
//

namespace UnityiOSCloud.KeyValue
{
    public enum CloudChangeReason
    {
        /// <summary>
        /// A value changed in iCloud. This occurs when another device, running another instance of
        /// your app and attached to the same iCloud account, uploads a new value.
        /// </summary>
        ServerChange = 0,

        /// <summary>
        /// Initial sync with iCloud server.
        /// </summary>
        InitialSyncChange,

        /// <summary>
        /// Your app’s key-value store has exceeded its space quota on the iCloud server.
        /// </summary>
        QuotaViolationChange,

        /// <summary>
        /// The user has changed the primary iCloud account.
        /// The keys and values in the local key-value store have been replaced with those from the new account,
        /// regardless of the relative timestamps.
        /// </summary>
        AccountChange
    }
}