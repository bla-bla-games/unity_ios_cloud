﻿
//
//  KeyValueStorageBridge.cs
//
//  Product: UnityiOSCloud
//  Author: Sergey Smirnov
//  Support: d.duck.dev@gmail.com
//

using System;
using System.Runtime.InteropServices;
using AOT;

namespace UnityiOSCloud.KeyValue
{
    internal class KeyValueStorageBridge
    {
        private class NativeAPI
        {
            #region Native General Interface
            [DllImport("__Internal")]
            internal static extern void _native_removeValue(string key);

            [DllImport("__Internal")]
            internal static extern bool _native_synchronize();
            #endregion

            #region Native Activation Interface
            [DllImport("__Internal")]
            internal static extern void _native_activate(KVStorageChangedCallback callback);

            [DllImport("__Internal")]
            internal static extern void _native_deactivate();
            #endregion

            #region Native Set Interface
            [DllImport("__Internal")]
            internal static extern void _native_setString(string key, string value);

            [DllImport("__Internal")]
            internal static extern void _native_setLong(string key, long value);

            [DllImport("__Internal")]
            internal static extern void _native_setDouble(string key, double value);

            [DllImport("__Internal")]
            internal static extern void _native_setBool(string key, bool value);
            #endregion

            #region Native Get Interface
            [DllImport("__Internal")]
            internal static extern IntPtr _native_getString(string key);

            [DllImport("__Internal")]
            internal static extern long _native_getLong(string key);

            [DllImport("__Internal")]
            internal static extern double _native_getDouble(string key);

            [DllImport("__Internal")]
            internal static extern bool _native_getBool(string key);
            #endregion
        }

        #region Callback Delegates
        internal delegate void KVStorageChangedCallback(string value);
        #endregion

        private static IKVSContext _context;

        #region Managed General Interface
        internal static void Initialize(IKVSContext context)
        {
            _context = context;
        }

        internal static void RemoveValue(string key)
        {
            NativeAPI._native_removeValue(key);
        }

        internal static bool Synchronize()
        {
            return NativeAPI._native_synchronize();
        }
        #endregion

        #region Managed Activation Interface
        internal static void Activate()
        {
            NativeAPI._native_activate(OnKVStorageWasChanged);
        }

        internal static void Deactivate()
        {
            NativeAPI._native_deactivate();
        }
        #endregion

        #region Managed Set Interface
        internal static void SetString(string key, string value)
        {
            NativeAPI._native_setString(key, value);
        }

        internal static void SetLong(string key, long value)
        {
            NativeAPI._native_setLong(key, value);
        }

        internal static void SetDouble(string key, double value)
        {
            NativeAPI._native_setDouble(key, value);
        }

        internal static void SetBool(string key, bool value)
        {
            NativeAPI._native_setBool(key, value);
        }

        internal static void SetData(string key, byte[] value)
        {
            string base64 = Convert.ToBase64String(value);
            NativeAPI._native_setString(key, base64);
        }
        #endregion

        #region Managed Get Interface
        internal static string GetString(string key)
        {
            IntPtr ptr = NativeAPI._native_getString(key);
            return ParseString(ptr);
        }

        internal static long GetLong(string key)
        {
            return NativeAPI._native_getLong(key);
        }

        internal static double GetDouble(string key)
        {
            return NativeAPI._native_getDouble(key);
        }

        internal static bool GetBool(string key)
        {
            return NativeAPI._native_getBool(key);
        }

        internal static byte[] GetData(string key)
        {
            string base64 = GetString(key);
            return iOSCloudUtilities.ParseData(base64);
        }
        #endregion

        #region Callbacks
        [MonoPInvokeCallback(typeof(KVStorageChangedCallback))]
        private static void OnKVStorageWasChanged(string rawData)
        {
            _context.KVStorageWasChanged(rawData);
        }
        #endregion

        private static string ParseString(IntPtr ptr)
        {
            string result = Marshal.PtrToStringAuto(ptr);

            // free memory allocated from unmanaged code
            Marshal.FreeHGlobal(ptr);

            return result;
        }
    }
}