﻿//
//  KeyValueStorage.cs
//
//  Product: UnityiOSCloud
//  Author: Sergey Smirnov
//  Support: d.duck.dev@gmail.com
//

using System;
using UnityEngine;

namespace UnityiOSCloud.KeyValue
{
    /// <summary>
    /// Key-value storage is similar to the local PlayerPrefs database, but values that you place
    /// in key-value storage are available to every instance of your app on all of a user’s various devices.
    /// If one instance of your app changes a value, the other instances see that change and can use it to update
    /// their configuration.
    /// </summary>
    public class KeyValueStorage : IKVSContext
    {
        public event Action<CloudChangeNotification> OnKVStorageWasChanged;

        internal KeyValueStorage()
        {
            KeyValueStorageBridge.Initialize(this);
        }

        #region General Interface

        /// <summary>
        /// Removes the value associated with the specified key from the key-value store.
        /// </summary>
        /// <param name="key">The key corresponding to the value you want to remove.</param>
        public void RemoveValue(string key)
        {
            KeyValueStorageBridge.RemoveValue(key);
        }

        /// <summary>
        /// Explicitly synchronizes in-memory keys and values with those stored on disk.
        /// </summary>
        /// <returns>true if the in-memory and on-disk keys and values were synchronized, or false if an error occurred.</returns>
        public bool Synchronize()
        {
            return KeyValueStorageBridge.Synchronize();
        }
        #endregion

        #region Subscription Interface

        /// <summary>
        /// Activates Key Value Storage.
        /// Subscribes for Key Value storage notifications. You can reveive these notification via <c>OnKVStorageWasChanged</c> event.
        /// </summary>
        public void Activate()
        {
            KeyValueStorageBridge.Activate();
        }

        /// <summary>
        /// Deactivates Key Value Storage.
        /// Unsubscribes from Key Value storage notifications.
        /// </summary>
        public void Deactivate()
        {
            KeyValueStorageBridge.Deactivate();
        }
        #endregion

        #region Set Interface

        /// <summary>
        /// Sets a string object for the specified key in the key-value store.
        /// </summary>
        /// <param name="key">The key under which to store the value.</param>
        /// <param name="value">The string you want to store. The total size (in bytes) of the string must not exceed the per-key size limits.</param>
        public void SetString(string key, string value)
        {
            KeyValueStorageBridge.SetString(key, value);
        }

        /// <summary>
        /// Sets a int value for the specified key in the key-value store.
        /// </summary>
        /// <param name="key">The key under which to store the value.</param>
        /// <param name="value">The int value to store.</param>
        public void SetInt(string key, int value)
        {
            KeyValueStorageBridge.SetLong(key, value);
        }

        /// <summary>
        /// Sets a long value for the specified key in the key-value store.
        /// </summary>
        /// <param name="key">The key under which to store the value.</param>
        /// <param name="value">The long value to store.</param>
        public void SetLong(string key, long value)
        {
            KeyValueStorageBridge.SetLong(key, value);
        }

        /// <summary>
        /// Sets a float value for the specified key in the key-value store.
        /// </summary>
        /// <param name="key">The key under which to store the value.</param>
        /// <param name="value">The float value to store.</param>
        public void SetFloat(string key, float value)
        {
            KeyValueStorageBridge.SetDouble(key, value);
        }

        /// <summary>
        /// Sets a double value for the specified key in the key-value store.
        /// </summary>
        /// <param name="key">The key under which to store the value.</param>
        /// <param name="value">The double value to store.</param>
        public void SetDouble(string key, double value)
        {
            KeyValueStorageBridge.SetDouble(key, value);
        }

        /// <summary>
        /// Sets a Boolean value for the specified key in the key-value store.
        /// </summary>
        /// <param name="key">The Boolean value to store.</param>
        /// <param name="value">The key under which to store the value.</param>
        public void SetBool(string key, bool value)
        {
            KeyValueStorageBridge.SetBool(key, value);
        }

        /// <summary>
        /// Sets a Color value for the specified key in the key-value store.
        /// </summary>
        /// <param name="key">The key under which to store the value.</param>
        /// <param name="value">The Color value to store.</param>
        public void SetColor(string key, Color value)
        {
            KeyValueStorageBridge.SetString(key, ColorUtility.ToHtmlStringRGB(value));
        }

        /// <summary>
        /// Sets a Color32 value for the specified key in the key-value store.
        /// </summary>
        /// <param name="key">The key under which to store the value.</param>
        /// <param name="value">The Color32 value to store.</param>
        public void SetColor32(string key, Color32 value)
        {
            KeyValueStorageBridge.SetString(key, ColorUtility.ToHtmlStringRGBA(value));
        }

        /// <summary>
        /// Sets a byte[] value for the specified key in the key-value store.
        /// </summary>
        /// <param name="key">The key under which to store the value.</param>
        /// <param name="value">The byte[] value to store.</param>
        public void SetData(string key, byte[] value)
        {
            KeyValueStorageBridge.SetData(key, value);
        }
        #endregion

        #region Get Interface

        /// <summary>
        /// Returns the string associated with the specified key.
        /// </summary>
        /// <param name="key">A key in the key-value store.</param>
        /// <returns>The string associated with the specified key or null.</returns>
        public string GetString(string key)
        {
            return KeyValueStorageBridge.GetString(key);
        }

        /// <summary>
        /// Returns the int value associated with the specified key.
        /// </summary>
        /// <param name="key">A key in the key-value store.</param>
        /// <returns>The int value associated with the specified key or 0 if the key was not found.</returns>
        public int GetInt(string key)
        {
            return (int)KeyValueStorageBridge.GetLong(key);
        }

        /// <summary>
        /// Returns the long value associated with the specified key.
        /// </summary>
        /// <param name="key">A key in the key-value store.</param>
        /// <returns>The long value associated with the specified key or 0 if the key was not found.</returns>
        public long GetLong(string key)
        {
            return KeyValueStorageBridge.GetLong(key);
        }

        /// <summary>
        /// Returns the float value associated with the specified key.
        /// </summary>
        /// <param name="key">A key in the key-value store.</param>
        /// <returns>The float value associated with the specified key or 0 if the key was not found.</returns>
        public float GetFloat(string key)
        {
            return (float)KeyValueStorageBridge.GetDouble(key);
        }

        /// <summary>
        /// Returns the double value associated with the specified key.
        /// </summary>
        /// <param name="key">A key in the key-value store.</param>
        /// <returns>The double value associated with the specified key or 0 if the key was not found.</returns>
        public double GetDouble(string key)
        {
            return KeyValueStorageBridge.GetDouble(key);
        }

        /// <summary>
        /// Returns the Boolean value associated with the specified key.
        /// </summary>
        /// <param name="key">A key in the key-value store.</param>
        /// <returns>If a Boolean value is associated with the specified key, that value is returned. If the key was not found, this method returns false.</returns>
        public bool GetBool(string key)
        {
            return KeyValueStorageBridge.GetBool(key);
        }

        /// <summary>
        /// Returns the Color value associated with the specified key.
        /// </summary>
        /// <param name="key">A key in the key-value store.</param>
        /// <returns>The Color value associated with the specified key or default Color if the key was not found or value was not parsed.</returns>
        public Color GetColor(string key)
        {
            return iOSCloudUtilities.ParseColor(KeyValueStorageBridge.GetString(key));
        }

        /// <summary>
        /// Returns the Color32 value associated with the specified key.
        /// </summary>
        /// <param name="key">A key in the key-value store.</param>
        /// <returns>The Color32 value associated with the specified key or default Color if the key was not found or value was not parsed.</returns>
        public Color32 GetColor32(string key)
        {
            return iOSCloudUtilities.ParseColor(KeyValueStorageBridge.GetString(key));
        }

        /// <summary>
        /// Returns the byte[] value associated with the specified key.
        /// </summary>
        /// <param name="key">A key in the key-value store.</param>
        /// <returns>The byte[] value associated with the specified key.</returns>
        public byte[] GetData(string key)
        {
            string rawData = GetString(key);
            return iOSCloudUtilities.ParseData(rawData);
        }
        #endregion

        public void KVStorageWasChanged(string rawData)
        {
            if (OnKVStorageWasChanged != null)
            {
                CloudChangeNotification notification = CreateNotification(rawData);
                OnKVStorageWasChanged(notification);
            }
        }

        private CloudChangeNotification CreateNotification(string rawData)
        {
            return JsonUtility.FromJson<CloudChangeNotification>(rawData);
        }
    }
}