﻿//
//  IKVSContext.cs
//
//  Product: UnityiOSCloud
//  Author: Sergey Smirnov
//  Support: d.duck.dev@gmail.com
//

namespace UnityiOSCloud.KeyValue
{
    public interface IKVSContext
    {
        void KVStorageWasChanged(string rawData);
    }
}