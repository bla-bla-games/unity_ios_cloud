﻿//
//  CloudChangeNotification.cs
//
//  Product: UnityiOSCloud
//  Author: Sergey Smirnov
//  Support: d.duck.dev@gmail.com
//

using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityiOSCloud.KeyValue
{
    [Serializable]
    public class CloudChangeNotification
    {
        [SerializeField]
        private CloudChangeReason _reason;

        [SerializeField]
        private List<iOSCloudFieldPair> _changedValues;

        /// <summary>
        /// Indicates the reason the key-value store changed.
        /// </summary>
        public CloudChangeReason Reason
        {
            get { return _reason; }
        }

        /// <summary>
        /// List of key-value store changes.
        /// </summary>
        public List<iOSCloudFieldPair> ChangedValues
        {
            get { return _changedValues; }
        }
    }
}